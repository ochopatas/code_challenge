const path = require('path');
const express = require('express');
const httpRequest = require('request-promise');
const PORT = 3000;

var LAUNCH_LIST = [];

// Create new express server
const app = express();
const server = require('http').Server(app);

server.listen(PORT, function () {
    console.log('Listening on Port 3000. Open and accepting connections.');

    //We can also get launch data on server start as well by making request here.
});

app.use(express.static(__dirname + '/public'))

app.get('/', function(request, response) {
    response.sendFile(path.join(__dirname + '/index.html'));
})

// Get up-to-date launch data.
app.get('/api/launches', function(request, response) {
    //Due to 403 from Launch Library errors when making generic request, need to specify Accept and User-Agent headers to retrieve data.
    var headerOptions = {
        uri: 'https://launchlibrary.net/1.2/launch?mode=verbose',
        headers: {
            'Accept': 'application/json',
            'User-Agent': 'Request-Promise'
        },
        json: true //pre-parse JSON
    };

    httpRequest(headerOptions)
        .then(onGetLaunchData)
        .catch(function (error) {
            console.log('Error getting data from Launch Library');
        })
        .then(function() {
            response.end(JSON.stringify(LAUNCH_LIST));
        })
});

var onGetLaunchData = function(body) {
    console.log('Getting Up to Date Launch information');

    LAUNCH_LIST = [];

    body.launches.forEach(function(launch) {
        var launchData = {
            id: launch.id,
            rocketName: launch.rocket.name,
            agencies: launch.rocket.agencies.map(function (obj) { return obj.name }),
            launchLocation: launch.location.name,
            launchTime: launch.windowstart
        };

        LAUNCH_LIST.push(launchData);
    });
}