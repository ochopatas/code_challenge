LAUNCH_DATA = {};

/* DOMs */
var listItem = document.getElementById('listItemTemplate');
var listWindow = document.getElementById('listWindow');

window.onload = function() {
    initializeData(populateList);
}

var initializeData = function(onInitializationSuccess) {
    makeHTTPGetRequest('/api/launches', function(response) { 
        LAUNCH_DATA = JSON.parse(response); 
        if (onInitializationSuccess)
            onInitializationSuccess();
    });  
}

var populateList = function() {        
    for (var i = 0; i < LAUNCH_DATA.length; i++) {
        var clone = listItem.cloneNode(true);
        clone.id = LAUNCH_DATA[i].id;
        var textFields = clone.getElementsByTagName('span');
        textFields[0].innerHTML = LAUNCH_DATA[i].rocketName;
        textFields[1].innerHTML = LAUNCH_DATA[i].launchLocation;
        textFields[2].innerHTML = LAUNCH_DATA[i].launchTime;
        textFields[3].innerHTML = LAUNCH_DATA[i].agencies;
        listWindow.appendChild(clone);
    }
}

/* Helper Functions */
var makeHTTPGetRequest = function(url, callbackSuccess) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState == XMLHttpRequest.DONE && httpRequest.status == 200) {
            if (callbackSuccess)
                callbackSuccess(httpRequest.responseText);
        }
    }

    httpRequest.open('GET', url, true);
    httpRequest.send();
}