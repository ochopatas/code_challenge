# Finn code challange
# Author: Sarah Sterchele
# Completed: August 3, 2017

To run: Type 'npm install && npm start' on command line. Alternatively also run 'node app.js' after installing modules.

Web app is accessible via: http://localhost:3000/.

Backend framework uses Express.  Front end is fairly vanilla JS with basic DOM API usage.